from werkzeug.security import generate_password_hash, check_password_hash
from flask import jsonify
from datetime import datetime
from flask_jwt_extended import create_access_token
from src.db import db

class UserModel(db.Model):
    """User account model."""

    __tablename__ = 'user_v0'
    id = db.Column(db.Integer,   primary_key=True)
    username = db.Column(db.String(100), nullable=False, unique=True)
    password = db.Column(db.String(200), nullable=False)

    created_on = db.Column(db.DateTime, index=False,
                           unique=False, nullable=True,)
    last_login = db.Column(db.DateTime, index=False,
                           unique=False, nullable=True)

    files_owned = db.relationship(
        "FilesModel", foreign_keys="FilesModel.created_by", back_populates="creator")
    # stories_owned = db.relationship("StoryModel" , foreign_keys="StoryModel.created_by", back_populates="creator")
    # stories_contrib = db.relationship("StoryModel", foreign_keys= "StoryModel.updated_by", back_populates="last_contributor")

    def __init__(self, username, password):
        self.username = username
        self.set_password(password)
        self.created_on = datetime.now()
        self.last_login = self.created_on

    def set_password(self, password):
        """Create hashed password."""
        self.password = generate_password_hash(
            password,
            method='sha256'
        )

    def check_password(self, password):
        """Check hashed password."""
        return check_password_hash(self.password, password)

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def json(self, include_details=False):
        result = {
            'id': self.id,
            'username': self.username,
            "joined": self.created_on.isoformat(),
            "last_accessed": self.last_login.isoformat(),
        }
        if include_details:
            result['stories_owned'] = [
                story.id for story in self.stories_owned]
            result["stories_contributed"] = [
                story.id for story in self.stories_contrib]
            result['words_owned'] = [word.word for word in self.words_owned]
            result["stories_contributed"] = [
                word.word for word in self.words_contrib]
        return result

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def find_by_username(cls, username):
        return cls.query.filter_by(username=username).first()

    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()
