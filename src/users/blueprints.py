from flask import Blueprint
from flask_restful import Api, Resource

from src.users.resources import TokenRefresh, User, UserInfo, UserLogin, UserLogout, UserRegister

user_bp = Blueprint('user_bp', __name__)

user_api = Api(user_bp)

user_api.add_resource(UserRegister, '/register')
user_api.add_resource(User,         '/<string:user_name>')
user_api.add_resource(UserLogin,    '/login')
user_api.add_resource(TokenRefresh, '/refresh')
user_api.add_resource(UserLogout,   '/logout')
user_api.add_resource(UserInfo,     '/myinfo')
