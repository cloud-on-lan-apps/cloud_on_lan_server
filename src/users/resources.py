from flask_restful import Resource, reqparse
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    get_current_user,
    get_jwt_identity,
    jwt_required,
    get_jwt,
)

from .models import UserModel
from src.blocklist import BLOCKLIST

_user_parser = reqparse.RequestParser()
_user_parser.add_argument(
    "username", type=str, required=True, help="This field cannot be blank."
)
_user_parser.add_argument(
    "password", type=str, required=True, help="This field cannot be blank."
)


class UserRegister(Resource):
    def post(self):
        data = _user_parser.parse_args()
        if UserModel.find_by_username(data["username"]):
            return {"message": "A user with that username already exists"}, 400
        user = UserModel(**data)
        user.save_to_db()
        return {"success": "User created successfully."}, 201


class UserLogin(Resource):
    def post(self):
        data = _user_parser.parse_args()

        user = UserModel.find_by_username(data["username"])

        # this is what the `authenticate()` function did in security.py
        if user and user.check_password(data["password"]):
            # identity= is what the identity() function did in security.py—now stored in the JWT
            access_token = create_access_token(identity=user.id, fresh=True)
            refresh_token = create_refresh_token(user.id)

            return {"success": "User logged in",
                    "access_token": access_token,
                    "refresh_token": refresh_token, "user_id": user.id}, 200

        return {"message": "Invalid Credentials!"}, 401


class User(Resource):
    """
    This resource can be useful when testing our Flask app. We may not want to expose it to public users, but for the
    sake of demonstration in this course, it can be useful when we are manipulating data regarding the users.
    """

    @jwt_required()
    def get(cls, user_name: str):

        user = UserModel.find_by_username(user_name)
        if not user:
            return {"message": "User Not Found"}, 404
        current_user = UserModel.find_by_id(get_current_user())
        if (user.id != current_user.id):
            return {"message": "You can't request data of another user"}, 404

        return user.json(), 200

    @jwt_required()
    def delete(cls, user_name: str):
        user = UserModel.find_by_username(user_name)

        if not user:
            return {"message": "User Not Found"}, 404
        current_user = UserModel.find_by_id(get_current_user())
        if (user.id != current_user.id):
            return {"message": "You can't request data of another user"}, 404

        user.delete_from_db()
        return {"success": "User deleted."}, 200


class UserInfo(Resource):
    """
    This resource can be useful when testing our Flask app. We may not want to expose it to public users, but for the
    sake of demonstration in this course, it can be useful when we are manipulating data regarding the users.
    """

    @jwt_required()
    def get(cls):
        current_user = get_current_user()
        user = UserModel.find_by_id(current_user)
        if not user:
            return {"message": "User Not Found"}, 404
        return user.json(), 200


class UserLogout(Resource):
    @jwt_required()
    def post(self):
        # jti is "JWT ID", a unique identifier for a JWT.
        jti = get_jwt()["jti"]
        BLOCKLIST.add(jti)
        return {"success": "Successfully logged out"}, 200


class TokenRefresh(Resource):
    @jwt_required(refresh=True)
    def post(self):
        """
        Get a new access token without requiring username and password—only the 'refresh token'
        provided in the /login endpoint.

        Note that refreshed access tokens have a `fresh=False`, which means that the user may have not
        given us their username and password for potentially a long time (if the token has been
        refreshed many times over).
        """
        current_user = get_jwt_identity()
        new_token = create_access_token(identity=current_user, fresh=False)
        return {"access_token": new_token}, 200
