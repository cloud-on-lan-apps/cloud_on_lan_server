from datetime import timedelta
import os

# environment should contain
# export SQLALCHEMY_DATABASE_URI='mysql+pymysql://<username>:<password>@localhost/<DBNAME>'

class ConfigClass(object):
    # SQLALCHEMY_DATABASE_URI =
    SQLALCHEMY_DATABASE_URI = os.environ['SQLALCHEMY_DATABASE_URI']

    SQLALCHEMY_TRACK_MODIFICATIONS = False    # Avoids SQLAlchemy warning
    SECRET_KEY = 'secret!'
    JWT_SECRET_KEY = "super-secret"
    PROPAGATE_EXCEPTIONS = True
    JWT_BLACKLIST_ENABLED = True
    JWT_BLACKLIST_TOKEN_CHECKS = ['access', 'refresh']
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(hours=24)
    JWT_REFRESH_TOKEN_EXPIRES = timedelta(days=30)

    FILE_STORAGE_LOCATION = "/home/anandas/app_storage"
