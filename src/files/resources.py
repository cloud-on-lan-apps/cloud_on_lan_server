from flask import Flask, request
from flask_jwt_extended import (jwt_required, get_current_user)
from flask_restful import Api, Resource

from src.users.models import UserModel
from .models import FilesModel


class Upload(Resource):
    @jwt_required()
    def post(self):
        if 'file' not in request.files:
            return 'No file provided', 400

        file = request.files['file']
        if file.filename == '':
            return 'No file selected', 400

        current_user = get_current_user()
        """ 
        Check if the user already exists: The user might have deleted backdoor
        But the token is valid, hence user tries to post.
        """
        user_id = UserModel.find_by_id(current_user)
        if not user_id:
            return {"failed": f"Unable to find user with id {user_id}"}

        # file.save(os.path.join('path/to/archive/directory', file.filename))
        f = FilesModel.receive_file(fileObj=file, uploader_id=user_id)
        if f:
            return {"success": "File uploaded successfully.", "filename": file.filename}, 200
        else:
            return {"failed": "File upload Failed.", "filename": file.filename, "id": f.id}, 200
