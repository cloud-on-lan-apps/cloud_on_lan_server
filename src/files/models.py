from werkzeug.security import generate_password_hash, check_password_hash
from flask import jsonify
from datetime import datetime
from flask_jwt_extended import create_access_token
import os

from src.db import db
from src.config import ConfigClass

class FilesModel(db.Model):

    __private_key = object()

    __tablename__ = 'files_v0'
    id = db.Column(db.Integer,   primary_key=True)
    uploaded_on = db.Column(db.DateTime, index=False,
                            unique=False, nullable=True,)
    original_filename = db.Column(db.UnicodeText, nullable=False)
    info = db.Column(db.UnicodeText, nullable=False)

    created_by = db.Column(db.Integer, db.ForeignKey("user_v0.id"))
    creator = db.relationship(
        "UserModel", foreign_keys=created_by, back_populates="files_owned")

    def __init__(self, fileObj, uploader_id, private_key=None):
        if private_key != FilesModel.__private_key:
            raise ValueError(
                "Use Class Method  receive_file.")

        self.original_filename = fileObj.filename
        self.uploaded_on = datetime.now()
        self.creator = uploader_id
        self.info = "Nothing here"

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def receive_file(cls, fileObj, uploader_id):
        # TODO: Check if the file already exists.
        f = FilesModel(fileObj=fileObj, uploader_id=uploader_id,
                       private_key=cls.__private_key)
        if f:
            f.save_to_db()
        if f:
            file_path = os.path.join(
                ConfigClass.FILE_STORAGE_LOCATION, str(f.id))
            os.makedirs(f"{file_path}/.original",)
            fileObj.save(os.path.join(
                f"{file_path}/.original", fileObj.filename))

        return f
