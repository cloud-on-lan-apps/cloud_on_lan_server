from flask import Blueprint
from flask_restful import Api, Resource

from src.users.resources import TokenRefresh, User, UserInfo, UserLogin, UserLogout, UserRegister

from .resources import Upload

files_bp = Blueprint('files_bp', __name__)

api = Api(files_bp)
api.add_resource(Upload, '/upload')
