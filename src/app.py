from flask import (Flask,
                   jsonify,
                   request)
from flask_jwt_extended import (get_jwt_identity,
                                jwt_required,
                                JWTManager)
from flask_cors import CORS
import json
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy

from src.config import ConfigClass

from src.db import db
from src.blocklist import BLOCKLIST
from src.db import db
from src.users.blueprints import user_bp
from src.files.blueprints import files_bp


class AppServer():
    def __init__(self, ConfigCls):
        self.app = Flask(
            __name__,
        )
        CORS(self.app)
        self.app.config.from_object(ConfigCls)

        self.jwt = JWTManager(self.app)
        self.api = Api(self.app)

        db.init_app(self.app)
        with self.app.app_context():
            db.create_all()

    def setup(self):
        self.setup_jwt()

        @self.app.route('/', methods=['GET'])
        def default_route():
            return "No API defined for this endpoint. Use appropriate API call"

        self.setup_apiroutes()

    def setup_jwt(self):
        @self.jwt.additional_claims_loader
        # Remember identity is what we define when creating the access token
        def add_claims_to_jwt(identity):
            if identity == 1:   # instead of hard-coding, we should read from a config file or database to get a list of admins instead
                return {'is_admin': True}
            return {'is_admin': False}

        # This method will check if a token is blacklisted, and will be called automatically when blacklist is enabled

        @self.jwt.token_in_blocklist_loader
        def check_if_token_in_blacklist(jwt_header, jwt_payload: dict):
            # Here we blacklist particular JWTs that have been created in the past.
            return jwt_payload["jti"] in BLOCKLIST

        # The following callbacks are used for customizing jwt response/error messages.
        # The original ones may not be in a very pretty format (opinionated)

        @self.jwt.expired_token_loader
        def expired_token_callback(jwt_header, jwt_payload: dict):
            return jsonify({
                'message': 'The token has expired.',
                'error': 'token_expired'
            }), 401

        @self.jwt.invalid_token_loader
        # we have to keep the argument here, since it's passed in by the caller internally
        def invalid_token_callback(error):
            return jsonify({
                'message': 'Signature verification failed.',
                'error': 'invalid_token'
            }), 401

        @self.jwt.unauthorized_loader
        def missing_token_callback(error):
            return jsonify({
                "message": "Request does not contain an access token.",
                'error': 'authorization_required'
            }), 401

        @self.jwt.needs_fresh_token_loader
        def token_not_fresh_callback():
            return jsonify({
                "message": "The token is not fresh.",
                'error': 'fresh_token_required'
            }), 401

        @self.jwt.revoked_token_loader
        def revoked_token_callback(_, __):
            return jsonify({
                "message": "The token has been revoked.",
                'error': 'token_revoked'
            }), 401

        @self.jwt.user_lookup_loader
        def user_lookup_callback(jwt_header, jwt_payload: dict):

            return jwt_payload.get('sub', None)
            pass

    def setup_apiroutes(self):
        self.app.register_blueprint(user_bp, url_prefix='/user')
        self.app.register_blueprint(files_bp, url_prefix='/file')


appserver = AppServer(ConfigClass)
appserver.setup()
app = appserver.app
