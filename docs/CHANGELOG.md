2023-06-8:

- Implementation of server begins.
- The server infrastructure with user management adopted from Read And Learn
- The DB is moved to mySQL
- Password security for MySQL moved to environment.
- Note:
  - Creating the MySQL DB and user must be done before starting the server
- Added TODO list, with immediate tasks
