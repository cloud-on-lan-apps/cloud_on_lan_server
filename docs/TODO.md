## Pending Tasks:

### design

- Table for Document, JSON Format

### implement

- Document upload Feature
  - [ ] Upload a document, process and gather needed info in a JSON and store it in DB
  - [ ] Store the image and connect it in DB
- Document List Feature
  - [ ] Finalize Queries for metadata to filterout available images and share back.

### Tests

- Write tests using httpie
  - [ ] User API testing
  - [ ] Upload API testing
  - [ ] Document list testing
  -
