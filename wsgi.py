from src.app import app

if __name__ == '__main__':
    print("Should not have been called?")
    app.run(debug=True, host="0.0.0.0", port=5002)
